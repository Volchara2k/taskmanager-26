package ru.renessans.jvschool.volkov.task.manager.repository;

import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.RepositoryImplementation;

public final class ServiceLocatorRepositoryTest {

    @NotNull
    private final IServiceLocatorRepository serviceLocatorRepository = new ServiceLocatorRepository();

    @Test
    @TestCaseName("Run testGetUserService for getUserService()")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    public void testGetUserService() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        @NotNull final IUserService userService = this.serviceLocatorRepository.getUserService();
        Assert.assertNotNull(userService);
    }

    @Test
    @TestCaseName("Run testGetSessionService for getSessionService()")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    public void testGetSessionService() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        @NotNull final ISessionService sessionService = this.serviceLocatorRepository.getSessionService();
        Assert.assertNotNull(sessionService);
    }

    @Test
    @TestCaseName("Run testGetAuthenticationService for getAuthenticationService()")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    public void testGetAuthenticationService() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        @NotNull final IAuthenticationService authService = this.serviceLocatorRepository.getAuthenticationService();
        Assert.assertNotNull(authService);
    }

    @Test
    @TestCaseName("Run testGetTaskService for getTaskService()")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    public void testGetTaskService() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        @NotNull final ITaskUserService taskService = this.serviceLocatorRepository.getTaskService();
        Assert.assertNotNull(taskService);
    }

    @Test
    @TestCaseName("Run testGetProjectService for getProjectService()")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    public void testGetProjectService() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        @NotNull final IProjectUserService projectService = this.serviceLocatorRepository.getProjectService();
        Assert.assertNotNull(projectService);
    }

    @Test
    @TestCaseName("Run testGetDataInterChangeService for getDataInterChangeService()")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    public void testGetDataInterChangeService() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        @NotNull final IDataInterChangeService dataService = this.serviceLocatorRepository.getDataInterChangeService();
        Assert.assertNotNull(dataService);
    }

    @Test
    @TestCaseName("Run testGetConfigurationService for getConfigurationService()")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    public void testGetConfigurationService() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        @NotNull final IConfigurationService configService = this.serviceLocatorRepository.getConfigurationService();
        Assert.assertNotNull(configService);
    }

}