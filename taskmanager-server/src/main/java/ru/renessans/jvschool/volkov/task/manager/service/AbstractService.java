package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.IRepository;
import ru.renessans.jvschool.volkov.task.manager.api.IService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEntityManagerFactoryService;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalAddModelException;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalDeleteModelException;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalSetAllModelException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.service.InvalidValueException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.service.InvalidValuesException;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractModel;
import ru.renessans.jvschool.volkov.task.manager.repository.Repository;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Objects;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class AbstractService<E extends AbstractModel> implements IService<E> {

    @NotNull
    private final IEntityManagerFactoryService managerFactoryService;

    @NotNull
    @SneakyThrows
    @Override
    public E persist(@Nullable final E value) {
        if (Objects.isNull(value)) throw new InvalidValueException();

        @NotNull final EntityManager entityManager = this.managerFactoryService.getEntityManager();
        @NotNull final IRepository<E> repository = new Repository<>(entityManager);

        @Nullable final E result;
        try {
            repository.beginTransaction();
            result = repository.persist(value);
            repository.commit();
        } catch (@NotNull final Exception exception) {
            repository.rollback();
            throw new IllegalAddModelException(exception.getCause());
        } finally {
            repository.close();
        }

        return result;
    }

    @NotNull
    @SneakyThrows
    @Override
    public E merge(@Nullable final E value) {
        if (Objects.isNull(value)) throw new InvalidValueException();

        @NotNull final EntityManager entityManager = this.managerFactoryService.getEntityManager();
        @NotNull final IRepository<E> repository = new Repository<>(entityManager);

        @Nullable final E result;
        try {
            repository.beginTransaction();
            result = repository.merge(value);
            repository.commit();
        } catch (@NotNull final Exception exception) {
            repository.rollback();
            throw new IllegalAddModelException(exception.getCause());
        } finally {
            repository.close();
        }

        return result;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Collection<E> setAllRecords(@Nullable final Collection<E> values) {
        if (ValidRuleUtil.isNullOrEmpty(values)) throw new InvalidValuesException();

        @NotNull final EntityManager entityManager = this.managerFactoryService.getEntityManager();
        @NotNull final IRepository<E> repository = new Repository<>(entityManager);

        @Nullable final Collection<E> result;
        try {
            repository.beginTransaction();
            result = repository.setAllRecords(values);
            repository.commit();
        } catch (@NotNull final Exception exception) {
            repository.rollback();
            throw new IllegalSetAllModelException(exception.getCause());
        } finally {
            repository.close();
        }

        return result;
    }

    @SneakyThrows
    @Override
    public boolean deletedRecord(@Nullable final E value) {
        if (Objects.isNull(value)) throw new InvalidValueException();

        @NotNull final EntityManager entityManager = this.managerFactoryService.getEntityManager();
        @NotNull final IRepository<E> repository = new Repository<>(entityManager);

        final boolean result;
        try {
            repository.beginTransaction();
            result = repository.deletedRecord(value);
            repository.commit();
        } catch (@NotNull final Exception exception) {
            repository.rollback();
            throw new IllegalDeleteModelException(exception.getCause());
        } finally {
            repository.close();
        }

        return result;
    }

    @SneakyThrows
    @Override
    public boolean deletedAllRecords() {
        @NotNull final EntityManager entityManager = this.managerFactoryService.getEntityManager();
        @NotNull final IRepository<E> repository = new Repository<>(entityManager);

        final boolean result;
        try {
            repository.beginTransaction();
            result = repository.deleteAllRecords();
            repository.commit();
        } catch (@NotNull final Exception exception) {
            repository.rollback();
            throw new IllegalDeleteModelException(exception.getCause());
        } finally {
            repository.close();
        }

        return result;
    }

}