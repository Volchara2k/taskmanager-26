package ru.renessans.jvschool.volkov.task.manager.exception.illegal;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class IllegalEntityManagerBuildingException extends AbstractException {

    @NotNull
    private static final String ENTITY_MANAGER_BUILDING_ILLEGAL =
            "Ошибка! Выполнение строительства менеджера управления сущностями завершился нелегальным образом!\n";

    public IllegalEntityManagerBuildingException(@NotNull final Throwable cause) {
        super(ENTITY_MANAGER_BUILDING_ILLEGAL, cause);
    }

}