package ru.renessans.jvschool.volkov.task.manager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.dto.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;

public interface IAdminDataInterChangeEndpoint {

    boolean dataBinClear(@Nullable SessionDTO sessionDTO);

    boolean dataBase64Clear(@Nullable SessionDTO sessionDTO);

    boolean dataJsonClear(@Nullable SessionDTO sessionDTO);

    boolean dataXmlClear(@Nullable SessionDTO sessionDTO);

    boolean dataYamlClear(@Nullable SessionDTO sessionDTO);

    @NotNull
    DomainDTO exportDataBin(@Nullable SessionDTO sessionDTO);

    @NotNull
    DomainDTO exportDataBase64(@Nullable SessionDTO sessionDTO);

    @NotNull
    DomainDTO exportDataJson(@Nullable SessionDTO sessionDTO);

    @NotNull
    DomainDTO exportDataXml(@Nullable SessionDTO sessionDTO);

    @NotNull
    DomainDTO exportDataYaml(@Nullable SessionDTO sessionDTO);

    @NotNull
    DomainDTO importDataBin(@Nullable SessionDTO sessionDTO);

    @NotNull
    DomainDTO importDataBase64(@Nullable SessionDTO sessionDTO);

    @NotNull
    DomainDTO importDataJson(@Nullable SessionDTO sessionDTO);

    @NotNull
    DomainDTO importDataXml(@Nullable SessionDTO sessionDTO);

    @NotNull
    DomainDTO importDataYaml(@Nullable SessionDTO sessionDTO);

}