package ru.renessans.jvschool.volkov.task.manager.exception.illegal;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class IllegalHashAlgorithmException extends AbstractException {

    @NotNull
    private static final String HASH_ALGORITHM_ILLEGAL =
            "Ошибка! Параметр \"алгоритм хеширования\" во внутренней реализации является нелегальным!\n";

    public IllegalHashAlgorithmException(@NotNull final Throwable throwable) {
        super(HASH_ALGORITHM_ILLEGAL, throwable);
    }

}