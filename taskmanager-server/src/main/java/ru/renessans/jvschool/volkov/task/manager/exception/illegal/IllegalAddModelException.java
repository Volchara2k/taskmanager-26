package ru.renessans.jvschool.volkov.task.manager.exception.illegal;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class IllegalAddModelException extends AbstractException {

    @NotNull
    private static final String MODEL_ADD_ILLEGAL =
            "Ошибка! Выполнение добавления модели в базе данных завершилось нелегальным образом!\n";

    public IllegalAddModelException(@NotNull final Throwable cause) {
        super(MODEL_ADD_ILLEGAL, cause);
    }

}