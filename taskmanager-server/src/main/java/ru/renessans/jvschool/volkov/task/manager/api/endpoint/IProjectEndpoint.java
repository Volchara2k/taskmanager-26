package ru.renessans.jvschool.volkov.task.manager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;

import java.util.Collection;

public interface IProjectEndpoint {

    @Nullable
    ProjectDTO addProject(
            @Nullable SessionDTO sessionDTO,
            @Nullable String title,
            @Nullable String description
    );

    @Nullable
    ProjectDTO updateProjectByIndex(
            @Nullable SessionDTO sessionDTO,
            @Nullable Integer index,
            @Nullable String title,
            @Nullable String description
    );

    @Nullable
    ProjectDTO updateProjectById(
            @Nullable SessionDTO sessionDTO,
            @Nullable String id,
            @Nullable String title,
            @Nullable String description
    );

    @Nullable
    ProjectDTO deleteProjectById(
            @Nullable SessionDTO sessionDTO,
            @Nullable String id
    );

    @Nullable
    ProjectDTO deleteProjectByIndex(
            @Nullable SessionDTO sessionDTO,
            @Nullable Integer index
    );

    @Nullable
    ProjectDTO deleteProjectByTitle(
            @Nullable SessionDTO sessionDTO,
            @Nullable String title
    );

    @NotNull
    Collection<ProjectDTO> deleteAllProjects(
            @Nullable SessionDTO sessionDTO
    );

    @Nullable
    ProjectDTO getProjectById(
            @Nullable SessionDTO sessionDTO,
            @Nullable String id
    );

    @Nullable
    ProjectDTO getProjectByIndex(
            @Nullable SessionDTO sessionDTO,
            @Nullable Integer index
    );

    @Nullable
    ProjectDTO getProjectByTitle(
            @Nullable SessionDTO sessionDTO,
            @Nullable String title
    );

    @NotNull
    Collection<ProjectDTO> getAllProjects(
            @Nullable SessionDTO sessionDTO
    );

}