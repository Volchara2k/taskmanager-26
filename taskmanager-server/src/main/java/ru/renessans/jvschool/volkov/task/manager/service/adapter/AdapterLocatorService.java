package ru.renessans.jvschool.volkov.task.manager.service.adapter;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IAdapterRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IAdapterLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IProjectAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ISessionAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ITaskAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IUserAdapterService;

@AllArgsConstructor
public final class AdapterLocatorService implements IAdapterLocatorService {

    private final IAdapterRepository adapterRepository;

    @NotNull
    public IProjectAdapterService getProjectAdapter() {
        return this.adapterRepository.getProjectAdapter();
    }

    @NotNull
    public ISessionAdapterService getSessionAdapter() {
        return this.adapterRepository.getSessionAdapter();
    }

    @NotNull
    public ITaskAdapterService getTaskAdapter() {
        return this.adapterRepository.getTaskAdapter();
    }

    @NotNull
    public IUserAdapterService getUserAdapter() {
        return this.adapterRepository.getUserAdapter();
    }

}