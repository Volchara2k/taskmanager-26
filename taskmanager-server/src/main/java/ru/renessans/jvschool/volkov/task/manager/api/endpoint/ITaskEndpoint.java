package ru.renessans.jvschool.volkov.task.manager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;

import java.util.Collection;

public interface ITaskEndpoint {

    @Nullable
    TaskDTO addTaskForProject(
            @Nullable SessionDTO sessionDTO,
            @Nullable String projectTitle,
            @Nullable String title,
            @Nullable String description
    );

    @Nullable
    TaskDTO addTask(
            @Nullable SessionDTO sessionDTO,
            @Nullable String title,
            @Nullable String description
    );

    @Nullable
    TaskDTO updateTaskById(
            @Nullable SessionDTO sessionDTO,
            @Nullable String id,
            @Nullable String title,
            @Nullable String description
    );

    @Nullable
    TaskDTO updateTaskByIndex(
            @Nullable SessionDTO sessionDTO,
            @Nullable Integer index,
            @Nullable String title,
            @Nullable String description
    );

    @Nullable
    TaskDTO deleteTaskById(
            @Nullable SessionDTO sessionDTO,
            @Nullable String id
    );

    @Nullable
    TaskDTO deleteTaskByIndex(
            @Nullable SessionDTO sessionDTO,
            @Nullable Integer index
    );

    @Nullable
    TaskDTO deleteTaskByTitle(
            @Nullable SessionDTO sessionDTO,
            @Nullable String title
    );

    @NotNull
    Collection<TaskDTO> deleteAllTasks(
            @Nullable SessionDTO sessionDTO
    );

    @Nullable
    TaskDTO getTaskById(
            @Nullable SessionDTO sessionDTO,
            @Nullable String id
    );

    @Nullable
    TaskDTO getTaskByIndex(
            @Nullable SessionDTO sessionDTO,
            @Nullable Integer index
    );

    @Nullable
    TaskDTO getTaskByTitle(
            @Nullable SessionDTO sessionDTO,
            @Nullable String title
    );

    @NotNull
    Collection<TaskDTO> getAllTasks(
            @Nullable SessionDTO sessionDTO
    );

}