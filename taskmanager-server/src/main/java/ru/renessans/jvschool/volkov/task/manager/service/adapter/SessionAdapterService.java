package ru.renessans.jvschool.volkov.task.manager.service.adapter;

import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ISessionAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.model.Session;

import java.util.Objects;

public final class SessionAdapterService implements ISessionAdapterService {

    @Nullable
    @Override
    public SessionDTO toDTO(@Nullable final Session convertible) {
        if (Objects.isNull(convertible)) return null;
        return SessionDTO.builder()
                .id(convertible.getId())
                .userId(convertible.getUserId())
                .timestamp(convertible.getTimestamp())
                .signature(convertible.getSignature())
                .build();
    }

    @Nullable
    @Override
    public Session toModel(@Nullable final SessionDTO convertible) {
        if (Objects.isNull(convertible)) return null;
        return Session.builder()
                .id(convertible.getId())
                .userId(convertible.getUserId())
                .timestamp(convertible.getTimestamp())
                .signature(convertible.getSignature())
                .build();
    }

}