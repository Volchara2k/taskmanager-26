package ru.renessans.jvschool.volkov.task.manager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.UserDTO;

public interface IUserEndpoint {

    @Nullable
    UserDTO getUser(
            @Nullable SessionDTO sessionDTO
    );

    @Nullable
    UserDTO editProfile(
            @Nullable SessionDTO sessionDTO,
            @Nullable String firstName
    );

    @Nullable
    UserDTO editProfileWithLastName(
            @Nullable SessionDTO sessionDTO,
            @Nullable String firstName,
            @Nullable String lastName
    );

    @Nullable
    UserDTO updatePassword(
            @Nullable SessionDTO sessionDTO,
            @Nullable String newPassword
    );

}