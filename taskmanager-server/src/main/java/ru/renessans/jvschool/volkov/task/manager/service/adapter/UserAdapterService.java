package ru.renessans.jvschool.volkov.task.manager.service.adapter;

import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IUserAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.UserDTO;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import java.util.Objects;

public final class UserAdapterService implements IUserAdapterService {

    @Nullable
    @Override
    public UserDTO toDTO(@Nullable final User convertible) {
        if (Objects.isNull(convertible)) return null;
        return UserDTO.builder()
                .id(convertible.getId())
                .login(convertible.getLogin())
                .passwordHash(convertible.getPasswordHash())
                .firstName(convertible.getFirstName())
                .lastName(convertible.getLastName())
                .middleName(convertible.getMiddleName())
                .role(convertible.getRole())
                .lockdown(convertible.getLockdown())
                .build();
    }

    @Nullable
    @Override
    public User toModel(@Nullable final UserDTO convertible) {
        if (Objects.isNull(convertible)) return null;
        return User.builder()
                .id(convertible.getId())
                .login(convertible.getLogin())
                .passwordHash(convertible.getPasswordHash())
                .firstName(convertible.getFirstName())
                .lastName(convertible.getLastName())
                .middleName(convertible.getMiddleName())
                .role(convertible.getRole())
                .lockdown(convertible.getLockdown())
                .build();
    }

}