package ru.renessans.jvschool.volkov.task.manager.exception.illegal;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class IllegalConfigurationLoadingException extends AbstractException {

    @NotNull
    private static final String CONFIG_LOADING_ILLEGAL =
            "Ошибка! Выполнение загрузки конфигурации приложения завершился нелегальным образом!\n";

    public IllegalConfigurationLoadingException(@NotNull final Throwable cause) {
        super(CONFIG_LOADING_ILLEGAL, cause);
    }

}