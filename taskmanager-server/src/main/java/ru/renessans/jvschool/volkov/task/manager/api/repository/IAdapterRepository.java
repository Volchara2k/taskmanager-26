package ru.renessans.jvschool.volkov.task.manager.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IProjectAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ISessionAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ITaskAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IUserAdapterService;

public interface IAdapterRepository {

    @NotNull
    IProjectAdapterService getProjectAdapter();

    @NotNull
    ISessionAdapterService getSessionAdapter();

    @NotNull
    ITaskAdapterService getTaskAdapter();

    @NotNull
    IUserAdapterService getUserAdapter();

}