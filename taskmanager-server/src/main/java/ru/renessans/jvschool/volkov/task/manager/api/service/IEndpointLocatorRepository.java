package ru.renessans.jvschool.volkov.task.manager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AbstractEndpoint;

import java.util.List;

public interface IEndpointLocatorRepository {

    @NotNull
    List<AbstractEndpoint> getAllEndpoints();

    @Nullable
    AbstractEndpoint getAuthEndpoint();

    @Nullable
    AbstractEndpoint getSessionEndpoint();

    @Nullable
    AbstractEndpoint getAdminEndpoint();

    @Nullable
    AbstractEndpoint getUserEndpoint();

    @Nullable
    AbstractEndpoint getTaskEndpoint();

    @Nullable
    AbstractEndpoint getProjectEndpoint();

    @Nullable
    AbstractEndpoint getDataEndpoint();

}