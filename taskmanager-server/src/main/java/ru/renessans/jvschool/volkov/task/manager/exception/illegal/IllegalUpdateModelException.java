package ru.renessans.jvschool.volkov.task.manager.exception.illegal;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class IllegalUpdateModelException extends AbstractException {

    @NotNull
    private static final String MODEL_UPDATE_ILLEGAL =
            "Ошибка! Выполнение обновления модели в базе данных завершилось нелегальным образом!\n";

    public IllegalUpdateModelException(@NotNull final Throwable cause) {
        super(MODEL_UPDATE_ILLEGAL, cause);
    }

}