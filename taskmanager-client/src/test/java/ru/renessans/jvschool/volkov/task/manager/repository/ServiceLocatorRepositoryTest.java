package ru.renessans.jvschool.volkov.task.manager.repository;

import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICommandService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.RepositoryImplementation;

public final class ServiceLocatorRepositoryTest {

    @NotNull
    private final IServiceLocatorRepository serviceLocatorRepository = new ServiceLocatorRepository();

    @Test
    @TestCaseName("Run testGetCurrentSession for getCurrentSession()")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    public void testGetCurrentSession() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        @NotNull final ICurrentSessionService currentSessionService = this.serviceLocatorRepository.getCurrentSession();
        Assert.assertNotNull(currentSessionService);
    }

    @Test
    @TestCaseName("Run testGetCommandService for getCommandService()")
    @Category({PositiveImplementation.class, RepositoryImplementation.class})
    public void testGetCommandService() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        @NotNull final ICommandService commandService = this.serviceLocatorRepository.getCommandService();
        Assert.assertNotNull(commandService);
    }

}