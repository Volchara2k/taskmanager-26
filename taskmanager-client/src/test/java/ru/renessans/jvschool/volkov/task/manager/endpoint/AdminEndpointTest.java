package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IEndpointLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.InternalDangerImplementation;
import ru.renessans.jvschool.volkov.task.manager.repository.EndpointLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.service.EndpointLocatorService;

import java.util.Collection;
import java.util.UUID;

@RunWith(value = JUnitParamsRunner.class)
@Category(IntegrationImplementation.class)
public final class AdminEndpointTest {

    @NotNull
    private final IEndpointLocatorRepository endpointLocatorRepository = new EndpointLocatorRepository();

    @NotNull
    private final IEndpointLocatorService endpointLocator = new EndpointLocatorService(endpointLocatorRepository);

    @NotNull
    private final SessionEndpoint sessionEndpoint = endpointLocator.getSessionEndpoint();

    @NotNull
    private final AuthenticationEndpoint authEndpoint = endpointLocator.getAuthenticationEndpoint();

    @NotNull
    private final AdminEndpoint adminEndpoint = endpointLocator.getAdminEndpoint();

    @Test
    @TestCaseName("Run testCloseAllSessions for closeAllSessions(session)")
    public void testCloseAllSessions() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        final boolean closeSessions = this.adminEndpoint.closeAllSessions(open);
        Assert.assertTrue(closeSessions);
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

    @Test
    @TestCaseName("Run testGetAllSessions for getAllSessions(session)")
    public void testGetAllSessions() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final Collection<SessionDTO> allSessions = this.adminEndpoint.getAllSessions(open);
        Assert.assertNotNull(allSessions);
        Assert.assertNotEquals(0, allSessions.size());
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

    @Test
    @TestCaseName("Run testSignUpUserWithUserRole for signUpUserWithUserRole(session, random, random)")
    public void testSignUpUserWithUserRole() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserDTO addUser = this.adminEndpoint.signUpUserWithUserRole(
                open, login, password, UserRole.USER
        );
        Assert.assertNotNull(addUser);
        Assert.assertEquals(login, addUser.getLogin());
        Assert.assertEquals(UserRole.USER, addUser.getRole());
        @NotNull final UserDTO deleteUser = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertNotNull(deleteUser);
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

    @Test
    @TestCaseName("Run testDeleteUserById for deleteUserById(session, id)")
    public void testDeleteUserById() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final UserDTO deleteUser = this.adminEndpoint.deleteUserById(open, addRecord.getId());
        Assert.assertNotNull(deleteUser);
        Assert.assertEquals(addRecord.getId(), deleteUser.getId());
        Assert.assertEquals(login, deleteUser.getLogin());
        Assert.assertEquals(addRecord.getRole(), deleteUser.getRole());
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

    @Test
    @TestCaseName("Run testDeleteUserByLogin for deleteUserByLogin(session, login)")
    public void testDeleteUserByLogin( ) {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final UserDTO deleteUser = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertNotNull(deleteUser);
        Assert.assertEquals(addRecord.getId(), deleteUser.getId());
        Assert.assertEquals(login, deleteUser.getLogin());
        Assert.assertEquals(addRecord.getRole(), deleteUser.getRole());
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

    @Test
    @Ignore("Делает работу остальных тестов некорректной")
    @Category(InternalDangerImplementation.class)
    @TestCaseName("Run testDeleteAllUsers for deleteAllUsers(session)")
    public void testDeleteAllUsers( ) {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        final boolean deletedAll = this.adminEndpoint.deleteAllUsers(open);
        Assert.assertTrue(deletedAll);
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

    @Test
    @TestCaseName("Run testLockUserByLogin for lockUserByLogin(session, login)")
    public void testLockUserByLogin() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final UserDTO lockUser = this.adminEndpoint.lockUserByLogin(open, login);
        Assert.assertNotNull(lockUser);
        Assert.assertEquals(addRecord.getId(), lockUser.getId());
        Assert.assertEquals(login, lockUser.getLogin());
        Assert.assertEquals(addRecord.getRole(), lockUser.getRole());
        @NotNull final UserDTO deleteUser = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertNotNull(deleteUser);
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

    @Test
    @TestCaseName("Run testUnlockUserByLogin for unlockUserByLogin(session, login)")
    public void testUnlockUserByLogin( ) {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @Nullable final UserDTO lockUser = this.adminEndpoint.lockUserByLogin(open, login);
        Assert.assertNotNull(lockUser);

        @Nullable final UserDTO unlockUser = this.adminEndpoint.unlockUserByLogin(open, login);
        Assert.assertNotNull(unlockUser);
        Assert.assertEquals(addRecord.getId(), unlockUser.getId());
        Assert.assertEquals(login, unlockUser.getLogin());
        Assert.assertEquals(addRecord.getRole(), unlockUser.getRole());
        @NotNull final UserDTO deleteUser = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertNotNull(deleteUser);
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

    @Test
    @TestCaseName("Run testGetAllUsers for getAllUsers(session)")
    public void testGetAllUsers() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final Collection<UserDTO> getUser = this.adminEndpoint.getAllUsers(open);
        Assert.assertNotNull(getUser);
        Assert.assertNotEquals(0, getUser.size());
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

    @Test
    @TestCaseName("Run testGetAllUsersTasks for getAllUsersTasks(session)")
    public void testGetAllUsersTasks() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final Collection<TaskDTO> allUsersTasks = this.adminEndpoint.getAllUsersTasks(open);
        Assert.assertNotNull(allUsersTasks);
        Assert.assertNotEquals(0, allUsersTasks.size());
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

    @Test
    @TestCaseName("Run testGetAllUsersProjects for getAllUsersProjects(session)")
    public void testGetAllUsersProjects( ) {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final Collection<ProjectDTO> allUsersProjects = this.adminEndpoint.getAllUsersProjects(open);
        Assert.assertNotNull(allUsersProjects);
        Assert.assertNotEquals(0, allUsersProjects.size());
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

    @Test
    @TestCaseName("Run testGetUserById for getUserById(session, login)")
    public void testGetUserById() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final UserDTO getUser = this.adminEndpoint.getUserById(open, addRecord.getId());
        Assert.assertNotNull(getUser);
        Assert.assertEquals(addRecord.getId(), getUser.getId());
        Assert.assertEquals(login, getUser.getLogin());
        @NotNull final UserDTO deleteUser = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertNotNull(deleteUser);
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

    @Test
    @TestCaseName("Run testGetUserByLogin for getUserByLogin(session, login)")
    public void testGetUserByLogin() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final UserDTO getUser = this.adminEndpoint.getUserByLogin(open, addRecord.getLogin());
        Assert.assertNotNull(getUser);
        Assert.assertEquals(addRecord.getId(), getUser.getId());
        Assert.assertEquals(login, getUser.getLogin());
        Assert.assertEquals(addRecord.getRole(), getUser.getRole());
        @NotNull final UserDTO deleteUser = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertNotNull(deleteUser);
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

    @Test
    @TestCaseName("Run testEditProfileById for editProfileById(session, id, \"newFirstName\")")
    public void testEditProfileById() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final UserDTO editUser = this.adminEndpoint.editProfileById(open, addRecord.getId(), "newFirstName");
        Assert.assertNotNull(editUser);
        Assert.assertEquals(addRecord.getId(), editUser.getId());
        Assert.assertEquals(login, editUser.getLogin());
        Assert.assertEquals("newFirstName", editUser.getFirstName());
        @NotNull final UserDTO deleteUser = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertNotNull(deleteUser);
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

    @Test
    @TestCaseName("Run testUpdatePasswordById for updatePasswordById(session, id, \"newPassword\")")
    public void testUpdatePasswordById() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.authEndpoint);
        Assert.assertNotNull(this.adminEndpoint);
        @NotNull final String login = UUID.randomUUID().toString();
        Assert.assertNotNull(login);
        @NotNull final String password = UUID.randomUUID().toString();
        Assert.assertNotNull(password);
        @NotNull final UserDTO addRecord = this.authEndpoint.signUpUser(null, login, password);
        Assert.assertNotNull(addRecord);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final UserDTO updatePassword = this.adminEndpoint.updatePasswordById(open, addRecord.getId(), "newPassword");
        Assert.assertNotNull(updatePassword);
        Assert.assertEquals(addRecord.getId(), updatePassword.getId());
        Assert.assertEquals(login, updatePassword.getLogin());
        @NotNull final UserDTO deleteUser = this.adminEndpoint.deleteUserByLogin(open, login);
        Assert.assertNotNull(deleteUser);
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

}