package ru.renessans.jvschool.volkov.task.manager.util;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataValidRuleProvider;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.UtilityImplementation;

import java.util.Collection;

@RunWith(value = JUnitParamsRunner.class)
public final class ValidRuleUtilTest {

    @Test
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @TestCaseName("Run testIsNotNullOrEmptyString: {0} for isNullOrEmpty(\"{1}\")")
    @Parameters(
            source = CaseDataValidRuleProvider.class,
            method = "validStringsCaseData"
    )
    public void testIsNotNullOrEmptyString(
            final boolean result,
            @NotNull final String validator
    ) {
        Assert.assertNotNull(validator);
        final boolean isNotNullOrEmptyString = ValidRuleUtil.isNotNullOrEmpty(validator);
        Assert.assertEquals(result, isNotNullOrEmptyString);
    }

    @Test
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @TestCaseName("Run testIsNullOrEmptyCollection: {0} for isNullOrEmpty({1})")
    @Parameters(
            source = CaseDataValidRuleProvider.class,
            method = "invalidCollectionsCaseData"
    )
    public void testIsNullOrEmptyCollection(
            final boolean result,
            @Nullable final Collection<Object> validator
    ) {
        final boolean isNotNullOrEmptyLong = ValidRuleUtil.isNullOrEmpty(validator);
        Assert.assertEquals(result, isNotNullOrEmptyLong);
    }

    @Test
    @Category({PositiveImplementation.class, UtilityImplementation.class})
    @TestCaseName("Run testIsNotNullOrEmptyCollection: {0} for isNullOrEmpty({1})")
    @Parameters(
            source = CaseDataValidRuleProvider.class,
            method = "validCollectionsCaseData"
    )
    public void testIsNotNullOrEmptyCollection(
            final boolean result,
            @NotNull final Collection<Object> validator
    ) {
        Assert.assertNotNull(validator);
        final boolean isNotNullOrEmptyLong = ValidRuleUtil.isNullOrEmpty(validator);
        Assert.assertEquals(result, isNotNullOrEmptyLong);
    }

}