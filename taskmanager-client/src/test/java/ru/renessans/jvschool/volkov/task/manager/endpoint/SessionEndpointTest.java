package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IEndpointLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;
import ru.renessans.jvschool.volkov.task.manager.repository.EndpointLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.service.EndpointLocatorService;

@RunWith(value = JUnitParamsRunner.class)
@Category(IntegrationImplementation.class)
public final class SessionEndpointTest {

    @NotNull
    private final IEndpointLocatorRepository endpointLocatorRepository = new EndpointLocatorRepository();

    @NotNull
    private final IEndpointLocatorService endpointLocator = new EndpointLocatorService(endpointLocatorRepository);

    @NotNull
    private final SessionEndpoint sessionEndpoint = endpointLocator.getSessionEndpoint();

    @Test
    @TestCaseName("Run testOpenSession for openSession(login, password)")
    public void testOpenSession() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

    @Test
    @TestCaseName("Run testCloseSession for closeSession(session)")
    public void testCloseSession() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

    @Test
    @TestCaseName("Run testVerifyValidSessionState for validateSession(session)")
    public void testVerifyValidSessionState() {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final SessionValidState verifyValidSessionState = this.sessionEndpoint.verifyValidSessionState(open);
        Assert.assertNotNull(verifyValidSessionState);
        Assert.assertEquals(SessionValidState.SUCCESS, verifyValidSessionState);
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

    @Test
    @TestCaseName("Run testVerifyValidPermissionState for verifyValidPermission(session, role)")
    public void testVerifyValidPermissionState( ) {
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.endpointLocator);
        Assert.assertNotNull(this.sessionEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final PermissionValidState permissionValidState =
                this.sessionEndpoint.verifyValidPermissionState(open, UserRole.ADMIN);
        Assert.assertNotNull(permissionValidState);
        Assert.assertEquals(PermissionValidState.SUCCESS, permissionValidState);
        final boolean closed = this.sessionEndpoint.closeSession(open);
        Assert.assertTrue(closed);
    }

}