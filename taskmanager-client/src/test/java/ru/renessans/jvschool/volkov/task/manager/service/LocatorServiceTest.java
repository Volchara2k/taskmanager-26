package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.renessans.jvschool.volkov.task.manager.api.ILocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IEndpointLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;
import ru.renessans.jvschool.volkov.task.manager.repository.EndpointLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.repository.ServiceLocatorRepository;

@Category(IntegrationImplementation.class)
public final class LocatorServiceTest {

    @NotNull
    private final IServiceLocatorRepository serviceLocatorRepository = new ServiceLocatorRepository();

    @NotNull
    private final IServiceLocatorService serviceLocator = new ServiceLocatorService(serviceLocatorRepository);

    @NotNull
    private final IEndpointLocatorRepository endpointLocatorRepository = new EndpointLocatorRepository();

    @NotNull
    private final IEndpointLocatorService endpointLocator = new EndpointLocatorService(endpointLocatorRepository);

    @NotNull
    private final ILocatorService locatorService = new LocatorService(
            endpointLocator, serviceLocator
    );

    @Test
    @TestCaseName("Run testGetEndpointLocator for getEndpointLocator()")
    public void testGetEndpointLocator() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.locatorService);
        @NotNull final IEndpointLocatorService endpointLocatorService = this.locatorService.getEndpointLocator();
        Assert.assertNotNull(endpointLocatorService);
    }

    @Test
    @TestCaseName("Run testGetServiceLocator for getServiceLocator()")
    public void testGetServiceLocator() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        Assert.assertNotNull(this.endpointLocatorRepository);
        Assert.assertNotNull(this.locatorService);
        @NotNull final IServiceLocatorService serviceLocatorService = this.locatorService.getServiceLocator();
        Assert.assertNotNull(serviceLocatorService);
    }

}