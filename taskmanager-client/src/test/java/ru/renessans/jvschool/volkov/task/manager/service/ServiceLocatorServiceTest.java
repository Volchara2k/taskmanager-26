package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.renessans.jvschool.volkov.task.manager.api.repository.IServiceLocatorRepository;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICommandService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.repository.ServiceLocatorRepository;

public final class ServiceLocatorServiceTest {

    @NotNull
    private final IServiceLocatorRepository serviceLocatorRepository = new ServiceLocatorRepository();

    @NotNull
    private final IServiceLocatorService serviceLocator = new ServiceLocatorService(serviceLocatorRepository);

    @Test
    @TestCaseName("Run testGetCurrentSession for getCurrentSession()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetCurrentSession() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        @NotNull final ICurrentSessionService currentSessionService = this.serviceLocator.getCurrentSession();
        Assert.assertNotNull(currentSessionService);
    }

    @Test
    @TestCaseName("Run testGetCommandService for getCommandService()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    public void testGetCommandService() {
        Assert.assertNotNull(this.serviceLocatorRepository);
        Assert.assertNotNull(this.serviceLocator);
        @NotNull final ICommandService commandService = this.serviceLocator.getCommandService();
        Assert.assertNotNull(commandService);
    }

}