package ru.renessans.jvschool.volkov.task.manager.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public final class SystemUtil {

    @NotNull
    private static final String HARDWARE_DATA_PATTERN =
            "Доступные процессоры (ядра): %d; \n" +
                    "Свободная память: %s; \n" +
                    "Максимальная память: %s; \n" +
                    "Общая память, доступная JVM: %s; \n" +
                    "Используемая память JVM: %s.";

    @NotNull
    public String getHardwareData() {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        @NotNull final String freeMemoryFormat = toFormatBytes(freeMemory);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        @NotNull final String totalMemoryFormat = toFormatBytes(totalMemory);

        final long usedMemory = totalMemory - freeMemory;
        @NotNull final String usedMemoryFormat = toFormatBytes(usedMemory);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        @NotNull final String maxMemoryValue = toFormatBytes(maxMemory);
        @NotNull final String maxMemoryFormat = (maxMemory == Long.MAX_VALUE ? "без ограничений" : maxMemoryValue);

        return String.format(
                HARDWARE_DATA_PATTERN,
                availableProcessors, freeMemoryFormat, maxMemoryFormat, totalMemoryFormat, usedMemoryFormat
        );
    }

    @NotNull
    private String toFormatBytes(final long bytes) {
        final long kilobyte = 1024;
        final long megabyte = kilobyte * 1024;
        final long gigabyte = megabyte * 1024;
        final long terabyte = gigabyte * 1024;

        if ((bytes >= 0) && (bytes < kilobyte)) {
            return bytes + " B";
        } else if ((bytes >= kilobyte) && (bytes < megabyte)) {
            return (bytes / kilobyte) + " KB";
        } else if ((bytes >= megabyte) && (bytes < gigabyte)) {
            return (bytes / megabyte) + " MB";
        } else if ((bytes >= gigabyte) && (bytes < terabyte)) {
            return (bytes / gigabyte) + " GB";
        } else if (bytes >= terabyte) {
            return (bytes / terabyte) + " TB";
        } else {
            return bytes + " Bytes";
        }
    }

}