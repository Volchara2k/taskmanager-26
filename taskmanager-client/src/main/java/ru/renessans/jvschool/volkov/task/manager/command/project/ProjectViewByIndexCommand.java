package ru.renessans.jvschool.volkov.task.manager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IEndpointLocatorService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@SuppressWarnings("unused")
public final class ProjectViewByIndexCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_PROJECT_VIEW_BY_INDEX = "project-view-by-index";

    @NotNull
    private static final String DESC_PROJECT_VIEW_BY_INDEX = "просмотреть проект по индексу";

    @NotNull
    private static final String NOTIFY_PROJECT_VIEW_BY_INDEX =
            "Происходит попытка инициализации отображения проекта. \n" +
                    "Для отображения проекта по индексу введите индекс проекта из списка. ";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_PROJECT_VIEW_BY_INDEX;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_PROJECT_VIEW_BY_INDEX;
    }

    @Override
    public void execute() {
        @NotNull final IServiceLocatorService serviceLocator = super.locatorService.getServiceLocator();
        @NotNull final ICurrentSessionService currentSessionService = serviceLocator.getCurrentSession();
        @Nullable final SessionDTO current = currentSessionService.getSession();

        ViewUtil.print(NOTIFY_PROJECT_VIEW_BY_INDEX);
        @NotNull final Integer index = ViewUtil.getInteger() - 1;

        @NotNull final IEndpointLocatorService endpointLocator = super.locatorService.getEndpointLocator();
        @NotNull final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();
        @Nullable final ProjectDTO project = projectEndpoint.getProjectByIndex(current, index);
        ViewUtil.print(project);
    }

}