package ru.renessans.jvschool.volkov.task.manager.command.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICommandService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IServiceLocatorService;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

import java.util.Collection;

@SuppressWarnings("unused")
public final class ArgumentListCommand extends AbstractCommand {

    @NotNull
    private static final String CMD_ARGUMENTS = "arguments";

    @NotNull
    private static final String ARG_ARGUMENTS = "-arg";

    @NotNull
    private static final String DESC_ARGUMENTS = "вывод списка поддерживаемых программных аргументов";

    @NotNull
    private static final String NOTIFY_ARGUMENTS = "Список поддерживаемых программных аргументов: \n";

    @NotNull
    @Override
    public String getCommand() {
        return CMD_ARGUMENTS;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARG_ARGUMENTS;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC_ARGUMENTS;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final IServiceLocatorService serviceLocator = super.locatorService.getServiceLocator();
        @NotNull final ICommandService commandService = serviceLocator.getCommandService();
        @Nullable final Collection<AbstractCommand> arguments = commandService.getAllArgumentCommands();
        ViewUtil.print(NOTIFY_ARGUMENTS);
        ViewUtil.print(arguments);
    }

}