
package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for dataBase64ClearResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="dataBase64ClearResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="isClearedBase64Data" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "dataBase64ClearResponse", propOrder = {
    "isClearedBase64Data"
})
public class DataBase64ClearResponse {

    protected boolean isClearedBase64Data;

    /**
     * Gets the value of the isClearedBase64Data property.
     * 
     */
    public boolean isIsClearedBase64Data() {
        return isClearedBase64Data;
    }

    /**
     * Sets the value of the isClearedBase64Data property.
     * 
     */
    public void setIsClearedBase64Data(boolean value) {
        this.isClearedBase64Data = value;
    }

}
