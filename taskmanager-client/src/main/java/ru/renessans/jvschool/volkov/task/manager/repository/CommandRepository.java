package ru.renessans.jvschool.volkov.task.manager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.renessans.jvschool.volkov.task.manager.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.task.manager.command.AbstractCommand;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalInstantiationException;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.lang.reflect.Modifier;
import java.util.*;
import java.util.stream.Collectors;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private static final String COMMAND_IMPLEMENTATION_PACKAGE = "ru.renessans.jvschool.volkov.task.manager.command";

    @NotNull
    private static final Collection<AbstractCommand> COMMAND_LIST = new ArrayList<>();

    @NotNull
    private final Map<String, AbstractCommand> commandMap = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> argumentMap = new LinkedHashMap<>();

    {
        fillCommandList();
        getAllTerminalCommands().forEach(command -> commandMap.put(command.getCommand(), command));
        getAllArgumentCommands().forEach(command -> argumentMap.put(command.getArgument(), command));
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getAllCommands() {
        return COMMAND_LIST;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getAllTerminalCommands() {
        return COMMAND_LIST
                .stream()
                .filter(command -> ValidRuleUtil.isNotNullOrEmpty(command.getCommand()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getAllArgumentCommands() {
        return COMMAND_LIST
                .stream()
                .filter(command -> ValidRuleUtil.isNotNullOrEmpty(command.getArgument()))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public AbstractCommand getTerminalCommand(@NotNull final String commandLine) {
        return this.commandMap.values()
                .stream()
                .filter(command -> commandLine.equals(command.getCommand()))
                .findAny()
                .orElse(null);
    }

    @Nullable
    @Override
    public AbstractCommand getArgumentCommand(@NotNull final String commandLine) {
        return this.argumentMap.values()
                .stream()
                .filter(command -> commandLine.equals(command.getArgument()))
                .findAny()
                .orElse(null);
    }

    @NotNull
    private Set<Class<? extends AbstractCommand>> extractCommandSet() {
        @NotNull final Reflections reflections = new Reflections(COMMAND_IMPLEMENTATION_PACKAGE);
        @NotNull final Set<Class<? extends AbstractCommand>> aClasses = reflections.getSubTypesOf(AbstractCommand.class);
        return aClasses;
    }

    private void fillCommandList() {
        @NotNull final Set<Class<? extends AbstractCommand>> classes = extractCommandSet();
        classes.forEach(aClass -> {
            final boolean isAbstractCommand = Modifier.isAbstract(aClass.getModifiers());
            if (isAbstractCommand) return;
            try {
                COMMAND_LIST.add(aClass.newInstance());
            } catch (@NotNull final Exception exception) {
                throw new IllegalInstantiationException();
            }
        });
    }

}