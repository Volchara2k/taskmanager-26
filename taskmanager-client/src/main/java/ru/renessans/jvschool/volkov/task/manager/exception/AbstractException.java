package ru.renessans.jvschool.volkov.task.manager.exception;

import org.jetbrains.annotations.NotNull;

public abstract class AbstractException extends Exception {

    public AbstractException(@NotNull final String message) {
        super(message);
    }

    public AbstractException(@NotNull final Throwable cause) {
        super(cause);
    }

}